import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Customer } from '../interface/customer.interface';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private _API = environment.apiUrl;

  constructor(private _http: HttpClient) {}

  //C
  addCustomer(customer: any) {
    return this._http.post<any>(`${this._API}/customer/save`, customer);
  }

  //R
  getCustomers() {
    return this._http.get<Customer>(`${this._API}/customer/all`);
  }

  //R
  getCustomer(id:number) {
    return this._http.get<any>(`${this._API}/customer/${id}`);
  }

  //U
  updateCustomer(id: number, customer: any) {
    return this._http.put<any>(`${this._API}/customer/${id}`, customer);
  }

  //D
  deleteCustomer(id: any) {
    return this._http.delete<any>(`${this._API}/customer/${id}`);
  }
}
