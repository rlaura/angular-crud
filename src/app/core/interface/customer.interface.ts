export interface Customer {
  id?:           number;
  companyName?:  string;
  contactName?:  string;
  contactTitle?: string;
  adress?:       string;
  city?:         string;
  region?:       string;
  postalCode?:   number;
  country?:      string;
  phone?:        number;
  fax?:          string;
}