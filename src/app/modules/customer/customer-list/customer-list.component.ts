import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from 'src/app/core/services/customer.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss'],
})
export class CustomerListComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'companyName',
    'contactName',
    'contactTitle',
    'adress',
    'city',
    'region',
    'postalCode',
    'country',
    'phone',
    'fax',
  ];
  dataSource: any = '';

  constructor(private customerService: CustomerService,private router:Router) {}

  ngOnInit(): void {
    this.listar();
  }

  listar() {
    this.customerService.getCustomers().subscribe((customers) => {
      //console.log(customers);
      this.dataSource = customers;
    });
  }

  borrar(id: number) {
    this.customerService.deleteCustomer(id).subscribe(data=>{
      this.listar();
    });

  }

  agregar() {
    this.router.navigate(['/customer/add'])
  }

  editar(id: number) {
    this.router.navigate(['/customer/edit/' + id])
  }
}
