import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { CustomerService } from 'src/app/core/services/customer.service';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.scss'],
})
export class CustomerAddComponent implements OnInit {
  
  miFormulario: FormGroup = this.fb.group({
    companyName: [, [Validators.required]],
    contactName: [, [Validators.required]],
    contactTitle: [, [Validators.required]],
    adress: [, [Validators.required]],
    city: [, [Validators.required]],
    region: [, [Validators.required]],
    postalCode: [, [Validators.required]],
    country: [, [Validators.required]],
    phone: [, [Validators.required]],
    fax: [, [Validators.required]],
  });

  dataCustomer: any = '';
  constructor(
    private customerService: CustomerService,
    private router: Router,
    private fb: FormBuilder,
    private activateRouter: ActivatedRoute
  ) {}

  ngOnInit(): void {

    if (!this.router.url.includes('edit')) {
      return;
    }

    this.activateRouter.params
      .pipe(switchMap(({ id }) => this.customerService.getCustomer(id)))
      .subscribe((customer) => {
        this.dataCustomer = customer;
        this.miFormulario.reset(this.dataCustomer);
      });
  }

  regresar() {
    this.router.navigate(['/customer/list']);
  }

  campoValido(campo: string) {
    return (
      this.miFormulario.get(campo)?.errors &&
      this.miFormulario.get(campo)?.touched
    );
  }

  guardar() {
    if (this.miFormulario.invalid) {
      console.log('formulario invalido');
      //marca todos los campos como tocados
      this.miFormulario.markAllAsTouched();
      return;
    }

    // console.log(this.miFormulario.value);
    if (this.dataCustomer.id) {
      this.customerService
        .updateCustomer(this.dataCustomer.id, this.miFormulario.value)
        .subscribe((data) => {
          this.miFormulario.reset();
          this.regresar();
        });

    } else {
      this.customerService
        .addCustomer(this.miFormulario.value)
        .subscribe((data) => {
          this.miFormulario.reset();
          this.regresar();
        });
    }
  }

  // actualizar(id: number) {
  //   this.customerService.updateCustomer(id, this.dataCustomer).subscribe();
  // }

  // traerData(id: number) {
  //   this.customerService.getCustomer(id).subscribe((data) => {
  //     this.dataCustomer = data;
  //   });
  // }
}
